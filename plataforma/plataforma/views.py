from flask import render_template, request
from pprint import pprint
from plataforma import app
import json


@app.route('/')
def index():
    return render_template('redirect_to_login.html')
    # app.logger.warning('sample message')
    # return render_template('index.html')


@app.route('/landing')
def landing():
    access_token = request.args.get('access_token')
    return render_template('landing.html', token=access_token)
