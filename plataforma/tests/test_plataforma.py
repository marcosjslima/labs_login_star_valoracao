import unittest

import plataforma


class PlataformaTestCase(unittest.TestCase):

    def setUp(self):
        self.app = plataforma.app.test_client()

    def test_index(self):
        rv = self.app.get('/')
        self.assertIn('Welcome to Plataforma', rv.data.decode())


if __name__ == '__main__':
    unittest.main()
