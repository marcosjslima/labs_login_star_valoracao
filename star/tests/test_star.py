import unittest

import star


class StarTestCase(unittest.TestCase):

    def setUp(self):
        self.app = star.app.test_client()

    def test_index(self):
        rv = self.app.get('/')
        self.assertIn('Welcome to Star', rv.data.decode())


if __name__ == '__main__':
    unittest.main()
